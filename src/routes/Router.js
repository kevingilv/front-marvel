import React from 'react';
import { Switch, Redirect, Route, Router } from 'react-router-dom'

import Login from '../modules/Login';
import TableCharacters from '../modules/characters/TableCharacters';
import Cart from '../modules/cart/Cart'
import NavbarPage from '../modules/NavbarPage';

const Main = () => (
    <main>
        <Switch>
            <Router exact path="/navbar" component={NavbarPage} />
            <Route exact path='/characters' component={TableCharacters} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path='/login' component={Login} />
            <Redirect to='/characters' />
        </Switch>
    </main>
)

export default Main;