import React from 'react';
import { Route } from 'react-router-dom';

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route {...rest} render={(props) => (
      <span><Component {...props} /> </span>
    )}
    />
  );
}

export default PrivateRoute;