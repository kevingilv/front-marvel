import axios from 'axios';
import * as constants from './constants';

const instance = axios.create({
    baseURL: constants.BASE_URL
});

instance.interceptors.request.use(config => {
    if (config.url !== '/user/login') {
        if (!instance.defaults.headers.common['Authorization']) {
            instance.defaults.headers.common['Authorization'] = `bearer ${sessionStorage.getItem(constants.TOKEN)}`;
            config.headers.common.Authorization = `bearer ${sessionStorage.getItem(constants.TOKEN)}`;
        }
        let currentDate = new Date().getTime() / 1000;
        if (parseFloat(currentDate) > parseFloat(sessionStorage.getItem('exp'))) {
            sessionStorage.clear();
            window.location.reload();
        }
    }
    return config;
});

export default instance;