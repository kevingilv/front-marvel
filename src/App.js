import React from 'react';
import Main from './routes/Router';
import './index';

function App() {
  return (
    <>
      <Main />
    </>
  );
}

export default App;
