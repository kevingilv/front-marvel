export const BASE_URL = "http://localhost:9000";
export const CART_CHARACTERS = "charCaractersSessionStorage";
export const ISLOGGED = "isLoggedKey";
export const TOKEN = "tokenConstant";
export const MARVEL_API_KEY = "4b882c2f1d5d0d594c618993c6580a68";
export const MARVEL_HASH = "a104a8ab6bc3362370c21beb003661b9";
export const EXPIRATION = 'exp';
export const DELETE_CHARACTER = "deleteCharacter";