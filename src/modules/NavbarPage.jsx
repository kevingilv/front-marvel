import React, { Component } from "react";
import {
  MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavbarToggler, MDBCollapse,
  MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon
} from "mdbreact";
import { BrowserRouter as Router } from 'react-router-dom';
import * as constants from '../constants';

class NavbarPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      showLogin: false
    }
  }

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <Router>
        <MDBNavbar color="black" dark expand="md">
          <MDBNavbarBrand>
            <strong className="red-text">MARVEL</strong>
            <strong className="white-text">|STUDIOS|</strong>
          </MDBNavbarBrand>
          <MDBNavbarToggler onClick={this.toggleCollapse} />
          <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
            <MDBNavbarNav left>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className="d-none d-md-inline">Options</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu className="dropdown-default">
                    <MDBDropdownItem href="/characters">List of characters</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
            </MDBNavbarNav>
            <MDBNavbarNav right>
              {sessionStorage.getItem(constants.ISLOGGED) ?
                <MDBNavItem>
                  <a className="btn btn-black btn-md" href="#!">
                    HI, YOU ARE LOGGED IN !!
                </a>
                </MDBNavItem> : null}
              <MDBNavItem>
                <a className="btn btn-dark btn-md" href="/cart">
                  CART
                  <span id="cartArticles" className="badge badge-danger ml-2">:D</span>
                </a>
              </MDBNavItem>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret className="mt-1">
                    <MDBIcon icon="user" />
                  </MDBDropdownToggle>
                  <MDBDropdownMenu className="dropdown-default">
                    {!sessionStorage.getItem(constants.ISLOGGED) ?
                      <MDBDropdownItem href='/login' >Login</MDBDropdownItem> :
                      <MDBDropdownItem onClick={() => this.logout()}>Logout</MDBDropdownItem>}
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBCollapse>
        </MDBNavbar>
      </Router >
    );

  }

  logout() {
    sessionStorage.removeItem(constants.TOKEN);
    sessionStorage.removeItem(constants.ISLOGGED);
    sessionStorage.removeItem(constants.CART_CHARACTERS);
    sessionStorage.removeItem(constants.DELETE_CHARACTER);
    alert('Bye bye...')
    window.location.reload();
  }

}

export default NavbarPage;