import React from 'react';
import {
    MDBBtn, MDBTable, MDBTableBody, MDBTableHead, MDBIcon, MDBBtnGroup,
    MDBContainer, MDBRow, MDBCol
} from 'mdbreact';
import * as constants from '../../constants';
import TableCharacters from '../characters/TableCharacters';
import axios from '../../axiosConfig';
import NavbarPage from '../NavbarPage';

class Cart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            columns: [
                { label: 'id', field: 'id', sort: 'asc' },
                { label: '', field: 'image', sort: 'asc' },
                { label: 'Name', field: 'name', sort: 'asc' },
                { label: 'Quantity', field: 'quantity', sort: 'asc' },
                { label: '', field: 'action', sort: 'asc' }
            ],
            marvelCharacters: [],
            hasChanges: false,
            deleted: []
        }
    }

    componentDidMount() {
        let existing = sessionStorage.getItem(constants.CART_CHARACTERS);
        existing = existing ? JSON.parse(existing) : [];

        if (sessionStorage.getItem(constants.ISLOGGED)) {
            axios.get('/cart/getbyid').then(res => {
                if (res.status === 200) {
                    sessionStorage.removeItem(constants.CART_CHARACTERS);
                    let insert = res.data.data;

                    if (insert.length > 0 && existing.length <= 0) {
                        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(insert));
                        this.fillCart(insert);
                        return;
                    }

                    if (existing.length > 0 && JSON.stringify(existing) !=
                        JSON.stringify(insert) && !this.state.hasChanges) {

                        insert.forEach(element => {
                            existing.push(element);
                        });

                        insert = existing.reduce(function (res, obj) {
                            if (!(obj.id in res))
                                res.__array.push(res[obj.id] = obj);
                            else {
                                res[obj.id].quantity += obj.quantity;
                            }
                            return res;
                        }, { __array: [] }).__array
                            .sort(function (a, b) { return b.id - a.id; });
                        this.setState({ hasChanges: true });
                        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(insert));
                        this.fillCart(insert);
                    } else {
                        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(existing));
                        this.fillCart(existing);
                    }

                } else {
                    alert("Oops! Something wrong...");
                }
            }).catch(err => {
                console.log(err);
                alert("Error 504!!");
            });
        } else if (existing.length > 0) {
            this.fillCart(existing);
        }
    }

    increaseQuantity(character) {
        this.fillCart(new TableCharacters().addCharacter(character));
        this.setState({ hasChanges: true });
    }

    decreaseQuantity(character) {
        let decrease = [];
        let newCart = [];
        let existing = sessionStorage.getItem(constants.CART_CHARACTERS);

        existing = existing ? JSON.parse(existing) : [];
        decrease = existing.filter(element => element.id === character.id);
        newCart = existing.filter(element => element.id !== character.id);
        newCart.push({ 'id': character.id, 'image': character.image, 'name': character.name, 'quantity': (decrease[0].quantity <= 1) ? 1 : decrease[0].quantity - 1 });
        newCart.sort((a, b) => (a.name > b.name) ? 1 : -1);
        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(newCart));
        this.setState({ hasChanges: true });

        this.fillCart(newCart);
    }


    fillCart(existing) {
        let tmpMarvelCharacters = [];
        existing.forEach(character => {
            tmpMarvelCharacters.push({
                'id': character.id,
                'image': <img src={character.image} className="image-size" alt="thumbnail"></img>,
                'name': character.name,
                'quantity': <MDBBtnGroup>
                    <MDBBtn color="dark" rounded size="sm" onClick={() =>
                        this.decreaseQuantity(character)}><MDBIcon icon="minus" /></MDBBtn>
                    <MDBBtn disabled color="white" rounded size="sm">{character.quantity}</MDBBtn>
                    <MDBBtn color="dark" rounded size="sm" onClick={() =>
                        this.increaseQuantity(character)}><MDBIcon icon="plus" /></MDBBtn>
                </MDBBtnGroup>,
                'action': <MDBBtn color="orange" rounded size="sm" onClick={() =>
                    this.deleteCharacter(character.id)}><MDBIcon icon="trash-alt" /></MDBBtn>
            });
        });
        this.setState({ marvelCharacters: tmpMarvelCharacters });
    }

    deleteCharacter(id) {

        if (sessionStorage.getItem(constants.ISLOGGED)) {
            let toDelete = sessionStorage.getItem(constants.DELETE_CHARACTER);
            if (toDelete != '' || toDelete != null) {
                sessionStorage.setItem(constants.DELETE_CHARACTER, `${toDelete},${id}`);
            }
        }
        let existing = sessionStorage.getItem(constants.CART_CHARACTERS);
        existing = existing ? JSON.parse(existing) : [];
        let newCart = existing.filter(element => element.id !== id);
        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(newCart));
        this.fillCart(newCart)
    }

    saveNewCart() {
        let toDelete = sessionStorage.getItem(constants.DELETE_CHARACTER);
        let payload = [];
        let existing = sessionStorage.getItem(constants.CART_CHARACTERS);
        existing = existing ? JSON.parse(existing) : [];
        if (existing.length > 0) {
            existing.forEach(element => {
                payload.push({
                    "characterId": element.id,
                    "characterImage": element.image,
                    "characterName": element.name,
                    "quantity": element.quantity
                });
            });
        }
        let arrayToDelete = (toDelete != null ? toDelete.split(',') : '');
        let deleteCharacterId = { 'deleteCharacterId': arrayToDelete };
        payload.push(deleteCharacterId);
        axios.post('/cart/add', payload).then(res => {
            if (res.status === 200) {
                alert('Cart updated!');
                sessionStorage.removeItem(constants.DELETE_CHARACTER);
                this.setState({ hasChanges: false })
            } else {
                alert("Oops! Something wrong...");
            }
        }).catch(err => {
            console.log(err);
            alert("Error 504!!");
        });


    }

    render() {
        if (this.state.marvelCharacters.length <= 0) {
            return (
                <>
                    <NavbarPage></NavbarPage>
                    <div className="center-container">
                        <img src="https://chocogrid.com/img/images/cart-empty.jpg" className="img-fluid" alt="" ></img>
                    </div>
                    {(sessionStorage.getItem(constants.ISLOGGED) && this.state.hasChanges) ?
                        <MDBBtn className="floating-button" color="green" onClick={() => this.saveNewCart()}>
                            SAVE <MDBIcon icon="save" className="ml-1" />
                        </MDBBtn> : null}
                </>
            )
        }
        return (
            <>
                <NavbarPage></NavbarPage>
                <MDBContainer className="main">
                    <MDBRow>
                        <MDBCol sm="12">
                            <h2>Cart</h2>
                            <MDBTable btn>
                                <MDBTableHead columns={this.state.columns} />
                                <MDBTableBody rows={this.state.marvelCharacters} />
                            </MDBTable >
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                {(sessionStorage.getItem(constants.ISLOGGED) && this.state.hasChanges) ?
                    <MDBBtn className="floating-button" color="green" onClick={() => this.saveNewCart()}>
                        SAVE <MDBIcon icon="save" className="ml-1" />
                    </MDBBtn> : null}
            </>
        )
    }


};

export default Cart;