import React from "react";
import '../styles/styles.css';
import {
    MDBContainer, MDBRow, MDBCol, MDBBtn, MDBCard,
    MDBCardBody, MDBIcon, MDBInput, MDBModalFooter
} from 'mdbreact';
import * as constants from '../constants';
import axios from '../axiosConfig';
import { Redirect } from "react-router";


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            showCharacters: false,
        }
    }

    render() {
        if (this.state.showCharacters) {
            return (
                <Redirect to="/characters" />
            )
        }

        return (
            <div className="center-container">
                <MDBRow>
                    <MDBCol sm="12">
                        <MDBContainer>
                            <MDBRow>
                                <MDBCol md="12">
                                    <MDBCard>
                                        <MDBCardBody>
                                            <form>
                                                <p className="h4 text-center py-4">Login</p>
                                                <div className="grey-text">
                                                    <MDBInput label="User" icon="user" group type="text" success="right"
                                                        value={this.state.username} onChange={(value) => this.setState({ username: value.target.value })} />

                                                    <MDBInput label="Password" icon="unlock" group type="password" success="right"
                                                        value={this.state.password} onChange={(value) => this.setState({ password: value.target.value })} />
                                                </div>
                                                <div className="text-center py-4 mt-3">
                                                    <MDBBtn className="btn btn-outline-red" onClick={() => this.login()}>
                                                        Send
                                                    <MDBIcon far icon="paper-plane" className="ml-2" />
                                                    </MDBBtn>
                                                </div>
                                            </form>
                                        </MDBCardBody>
                                        <MDBModalFooter className="mx-5 pt-3 mb-1">
                                            <p className="font-small grey-text d-flex justify-content-end">
                                                Not a member?
                                             <a href="/characters" className="blue-text ml-1">Continue without login</a>
                                            </p>
                                        </MDBModalFooter>
                                    </MDBCard>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                    </MDBCol>
                </MDBRow>
            </div>
        )
    }

    login() {
        if (!this.state.username == "" ||
            !this.state.password == "") {
            const datapost = {
                username: this.state.username,
                password: this.state.password
            }
            
            axios.post(`/user/login`, datapost)
                .then(response => {
                    if (response.data.success === true) {
                        let expirationDate = (new Date().getTime() / 1000) + 600;
                        sessionStorage.setItem(constants.TOKEN, response.data.data.token);
                        sessionStorage.setItem(constants.ISLOGGED, true);
                        sessionStorage.setItem(constants.EXPIRATION, expirationDate);

                        this.setState({ showCharacters: true });

                        //this.props.history.push('/characters');
                    } else if (response.data.message !== "") {
                        alert(response.data.message);
                    } else {
                        alert("Oops! Something wrong...");
                    }
                }).catch(error => {
                    console.log(error);
                    alert("Oops: Error 503");
                });
        } else {
            alert(`Fields can't be empty!`);
        }
    }
};

export default Login;