import React from 'react';
import {
    MDBBtn, MDBTable, MDBTableBody, MDBTableHead, MDBIcon,
    MDBContainer, MDBRow, MDBCol
} from 'mdbreact';

import axios from 'axios';
import * as constants from '../../constants';
import NavbarPage from '../NavbarPage';

class TableCharacters extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [],
            marvelCharacters: [],
            isLogged: false
        }
    }

    componentDidMount() {
        this.getMarvelCharacters();
    }

    render() {
        return (
            <>
                <NavbarPage></NavbarPage>
                <MDBContainer className="main">
                    <MDBRow>
                        <MDBCol sm="12">
                            <h2>List of Characters</h2>
                            <MDBTable btn>
                                <MDBTableHead columns={this.state.columns} />
                                <MDBTableBody rows={this.state.marvelCharacters} />
                            </MDBTable>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
            </>
        )
    }

    getMarvelCharacters() {
        let url = `https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=${constants.MARVEL_API_KEY}&hash=${constants.MARVEL_HASH}`;
        axios.get(url).then(res => {
            if (res.data.code === 200) {
                this.fillTable(res.data.data.results);
            } else {
                alert("Oops! Something wrong...");
            }
        }).catch(err => {
            console.log(err);
            alert("Error 504!!");
        });
    }

    fillTable(data) {
        let tmpMarvelCharacters = [];
        data.forEach(character => {
            let img = `${character.thumbnail.path}.${character.thumbnail.extension}`
            tmpMarvelCharacters.push({
                'id': character.id,
                'image': <img src={img} className="image-size" alt="thumbnail"></img>,
                'name': character.name,
                'description': (character.description !== "") ? character.description : "Not available",
                'action':
                    <MDBBtn color="dark" rounded size="sm" onClick={() =>
                        this.addCharacter(character)}>
                        <MDBIcon icon="plus" />
                    </MDBBtn>,
                'quantity': ''
            });
            this.setState({
                columns: [
                    { label: 'id', field: 'id' },
                    { label: '', field: 'image' },
                    { label: 'Name', field: 'name' },
                    { label: 'Description', field: 'description' },
                    { label: '', field: 'action' }
                ], marvelCharacters: tmpMarvelCharacters
            });
        });
    }

    addCharacter(character) {
        let img = '';
        let increase = [];
        let newCart = [];

        let existing = sessionStorage.getItem(constants.CART_CHARACTERS);
        existing = existing ? JSON.parse(existing) : [];
        increase = existing.filter(element => element.id === character.id);
        newCart = existing.filter(element => element.id !== character.id);
        if (typeof (character.thumbnail) !== 'undefined') {
            img = `${character.thumbnail.path}.${character.thumbnail.extension}`;
            newCart.push({ 'id': character.id, 'image': img, 'name': character.name, 'quantity': (increase.length <= 0) ? 1 : increase[0].quantity + 1 });
            alert('Item added to cart');
        } else {
            newCart.push({ 'id': character.id, 'image': character.image, 'name': character.name, 'quantity': (increase.length <= 0) ? 1 : increase[0].quantity + 1 });
        }
        newCart.sort((a, b) => (a.name > b.name) ? 1 : -1);
        sessionStorage.setItem(constants.CART_CHARACTERS, JSON.stringify(newCart));
        return newCart;

    }
};

export default TableCharacters;